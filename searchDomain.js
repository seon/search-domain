var thunkify = require('thunkify');
var request = require('request');
var logger = require('log4js').getLogger('search_domain');
var co = require('co');

var argv = process.argv;

var keyword = argv[2];

var len = argv[3] || (keyword.match(/\*+/) || [''])[0].length;

logger.info('search for: ' + keyword);

var pos = 'full';

if (keyword.match(/\$$/)) {
  pos = 'tail';
  keyword = keyword.replace(/\$$/, '');
} else if (keyword.match(/^\^/)) {
  pos = 'head';
  keyword = keyword.replace(/^\^/, '');
} else if (keyword.match(/\*+/)) {
  pos = 'replace';
}

var done = [];

var trying = [];

co(function*() {
  var sign =
    yield getSign();
  if (pos === 'full') {
    searchDomain = keyword;
    yield query(searchDomain, sign);
  } else {
    var joinLen = len;

    if (pos !== 'replace') {
      joinLen = len - keyword.length;
    }

    var joinChars = [];

    for (var i = 0; i < joinLen; i++) {
      joinChars.push('a');
    }
    while ((joinChars.join('').match(/z/g) || []).length < joinLen) {
      try {
        var searchDomain;
        if (pos === 'head') {
          searchDomain = keyword + joinChars.join('');
          yield query(searchDomain, sign);
        } else if (pos === 'tail') {
          searchDomain = joinChars.join('') + keyword;
          yield query(searchDomain, sign);
        } else if (pos === 'replace') {
          searchDomain = keyword.replace(/\*+/, joinChars.join(''));
          yield query(searchDomain, sign);
        } else {
          for (var i = 0; i <= joinLen; i++) {
            var arr = joinChars.slice(0);
            arr.splice.apply(arr, [i, 0].concat(keyword.split('')))
            searchDomain = arr.join('');
            yield query(searchDomain, sign);
          }
        }
        incr(joinChars);
      } catch (e) {
        logger.error(searchDomain);
        logger.error(e.stack)
        continue;
      }
    }
  }

}).then(function() {});

function incr(arr) {
  if (arr.join('').match(/[a-y]z/)) {
    while (arr.join('').match(/[a-y]z/)) {
      var pos = arr.indexOf('z');
      arr[pos] = 'a';
      arr[pos - 1] = String.fromCharCode(arr[pos - 1].charCodeAt(0) + 1);;
    }
  } else {
    arr[arr.length - 1] = String.fromCharCode(arr[arr.length - 1].charCodeAt(0) + 1);
  }
}

function* query(keyword, sign) {
  var queryUrl = 'http://other.domaincheck.cndns.com/domainchecks3.asp?id=ret8&checkdme=' + keyword + '.com&ttl=9&sign=' + sign + '&isname=&_=1423642737145';
  var result =
    yield thunkify(request)({
      url: queryUrl,
      timeout: 10000,
      headers: {
        Referer: 'http://www.cndns.com/cn/domain/full-dme-forms.asp',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'
      }
    });
  if (result) {
    var body = result[1];
    if (body) {
      body = body.replace(/^\(|\)$/g, '');
      eval('var json = ' + body);
      // console.log(json);
      if (json.result === 'ok') {
        logger.info('Valid: ' + keyword);
      } else {
        // console.log('.');
      }
    }
    // if(json)
  }
}

function* getSign() {
  var result =
    yield thunkify(request)({
      url: 'http://www.cndns.com/cn/domain/full-dme-forms.asp',
      method: 'POST',
      timeout: 10000,
      form: {
        search_dme_name: 'full',
        search_dme_suffix: 'com'
      }
    });
  if (result) {
    var body = result[1];
    var sign = body.match(/id="sign" value='(.+?)'/);
    if (sign) {
      return sign[1];
    }
  }
}